class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }
    get age() {
        return this._age;
    }
    set age(value) {
        this._age = value;
    }
    get salary() {
        return this._salary;
    }
    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
    get lang() {
        return this._lang;
    }
    set lang(value) {
        this._lang = value;
    }
    get salary() {
        return this._salary * 3;
    }

    set salary(value) {
        this._salary = value;
    }
}

const programmerJS = new Programmer('Vasya', 25, 1000, 'js');
console.log(programmerJS);
console.log(programmerJS.salary);

const programmerPHP = new Programmer('Petya', 30, 2000, 'php');
console.log(programmerPHP);
console.log(programmerPHP.salary);

const programmerJava = new Programmer('Kolya', 35, 6000, 'java');
console.log(programmerJava);
console.log(programmerJava.salary);
